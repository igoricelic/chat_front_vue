import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import CentralRoom from '@/components/chat/Chat'
import HelloWorld from '@/components/HelloWorld'
import Room from '@/components/room/Room'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/room',
      name: 'CentralRoom',
      component: CentralRoom
    },
    {
      path: '/channel',
      name: 'Room',
      component: Room
    },
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
