import axios from 'axios'
import { BACKEND_ADDRESS } from '../../config/index'

const axiosInstance = axios.create({
  baseURL: BACKEND_ADDRESS
})

const TOKEN_HEADER_KEY = 'X-AUTH-TOKEN'

axiosInstance.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

const authToken = window.localStorage.getItem('contactsapp_auth_token')

axiosInstance.defaults.headers.common[TOKEN_HEADER_KEY] = authToken ? authToken.toString() : ''

export default {
  ...axiosInstance,
  setAuthHeader (token) {
    this.defaults.headers.common[TOKEN_HEADER_KEY] = token ? token.toString() : ''
  }
}
