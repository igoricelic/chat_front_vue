import socketio from 'socket.io-client'

var socketInstance = socketio('http://localhost:3001')

export default {
  socketInstance
}
