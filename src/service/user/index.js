import http from '@/service/http'

export default {
  registerUser (object, cb, errorCb) {
    return http.post('/register', object)
      .then(res => cb(res.data))
      .catch(err => errorCb(err))
  },
  loginUser (object, cb, errorCb) {
    http.post('/login', object)
      .then(res => cb(res.data))
      .catch(err => errorCb(err))
  },
  logoutUser (cb, errorCb) {
    return http.get('/logout')
  }
}
