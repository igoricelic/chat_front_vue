const state = {
  emojis: [
    {
      'key': 'grinning_face',
      'value': '😀'
    },
    {
      'key': 'grimacing_face',
      'value': '😬'
    },
    {
      'key': 'grimacing_face_with_smile_eyes',
      'value': '😁'
    },
    {
      'key': 'face_with_tear_of_joy',
      'value': '😂'
    },
    {
      'key': 'smiling_face_with_open_mouth',
      'value': '😃'
    },
    {
      'key': 'smiling_face_with_open_mouth_eyes',
      'value': '😄'
    },
    {
      'key': 'smiling_face_with_open_mouth_cold_sweat',
      'value': '😅'
    },
    {
      'key': 'smiling_face_with_open_mouth_hand_tight',
      'value': '😆'
    },
    {
      'key': 'smiling_face_with_halo',
      'value': '😇'
    },
    {
      'key': 'winking_face',
      'value': '😉'
    },
    {
      'key': 'black_smiling_face',
      'value': '😊'
    },
    {
      'key': 'slightly_smiling_face',
      'value': '🙂'
    },
    {
      'key': 'upside_down_face',
      'value': '🙃'
    },
    {
      'key': 'white_smiling_face',
      'value': '☺'
    },
    {
      'key': 'face_savouring_delicious_food',
      'value': '😋'
    },
    {
      'key': 'relieved_face',
      'value': '😌'
    },
    {
      'key': 'smiling_face_heart_eyes',
      'value': '😍'
    },
    {
      'key': 'face_throwing_kiss',
      'value': '😘'
    },
    {
      'key': 'kissing_face',
      'value': '😗'
    },
    {
      'key': 'kissing_face_with_smile_eyes',
      'value': '😙'
    },
    {
      'key': 'kissing_face_with_closed_eyes',
      'value': '😚'
    },
    {
      'key': 'face_with_tongue_wink_eye',
      'value': '😜'
    },
    {
      'key': 'face_with_tongue_closed_eye',
      'value': '😝'
    },
    {
      'key': 'face_with_stuck_out_tongue',
      'value': '😛'
    },
    {
      'key': 'money_mouth_face',
      'value': '🤑'
    }
  ]
}

const mutations = {

}

const getters = {
  getEmojis: state => state.emojis
}

const actions = {

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
