import Vue from 'vue'
import Vuex from 'vuex'
import emoji from './emojes'
import messages from './messages'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    emoji,
    messages
  }
})
