import socket from '../../service/socket/index.js'

const state = {
  messages: []
}

const mutations = {
  receiveMessage (state, message) {
    state.messages.push(message)
  }
}

const getters = {
  // getMessages (state, channelName) {
  //   console.log('Pozvan sam')
  //   console.log(channelName)
  //   return state.messages.filter(message => message.channel === channelName)
  // }
  getMessages: state => state.messages
}

const actions = {
  sendMessage ({commit}, message) {
    socket.socketInstance.emit('SEND_MESSAGE', message)
    commit('receiveMessage', message)
  },
  postMessage ({commit}, message) {
    commit('receiveMessage', message)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
